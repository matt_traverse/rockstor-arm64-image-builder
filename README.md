# Rockstor base image for ARM64

This repository builds a "base" image for [Rockstor](http://rockstor.com) based
on openSuSE Leap 15.1 (soon 15.2).

As this is not a 'supported' release and there are no official rpm repositories for ARM64 anyway - users are required to run a script that will download and install Rockstor from source.

As per Rockstor source builds, you will not be able to upgrade directly to any future official release. These installs are unable to subscribe to stable channel updates.

## Supported hardware
* All ARM64 boards with EFI capabilities that already run openSuSE Leap 15.1

This image is based on the [Traverse](https://gitlab.com/traversetech/ls1088firmware/distribution-images) openSuSE customized image so contains some integration patches for Traverse Layerscape boards.

This image is tested regularly on the Ten64 board and under [muvirt](https://gitlab.com/traversetech/muvirt).

## Usage
The image has a default root password of "changethis" which you will be requred to change on the first login.

It is a good idea to change any local system settings - such as Time Zone, Locale and Network with YaST before installing Rockstor.

Run `/opt/rockstor-setup.sh` to download and install the current Rockstor development tree.

### Known issues
The image currently does not create a "share" for Rockstor itself - you will not be able to remove the first share you create as Rockstor will think it is the system share.

Only "core functionality" - creating shares and distributing by SMB/NFS/SFTP is tested at the moment. Rock-On's should work if the container is [multi-arch](https://www.docker.com/blog/multi-arch-build-and-images-the-simple-way/) - otherwise we will need a way to flag x86 or arm64 only containers.
 
## More information
* [Rockstor Source Development Guide](http://rockstor.com/docs/contribute.html#developers)
* [Rockstor openSuSE dev notes and status](https://forum.rockstor.com/t/built-on-opensuse-dev-notes-and-status/5662)
