#!/bin/sh
sed -i 's/ext4/btrfs/g' ../diskimage-builder/diskimage_builder/elements/block-device-efi/block-device-default.yaml

if [ ! $(grep DIB_BOOTLOADER_DEFAULT_CMDLINE ../diskimage-builder/tox.ini) ]; then
	  sed -i '/passenv=/a \\tDIB_BOOTLOADER_DEFAULT_CMDLINE' ../diskimage-builder/tox.ini
fi

rm -rf ../diskiamge-builder/diskimage_builder/elements/rockstor
cp -r rockstor ../diskimage-builder/diskimage_builder/elements

export APPSTORE_ID="rockstor-preview"
export APPSTORE_NAME="Rockstor ARM64 Preview (build from source)"
export APPSTORE_DESCRIPTION="Preview of Rockstor for ARM64 - on first you will need to run a build and install script. Upgrades to future stable releases are NOT supported"

EXTRA_ELEMENTS="rockstor" IMAGE_BASE_NAME="rockstor" DIB_BOOTLOADER_DEFAULT_CMDLINE="ipv6.disable=1" ../opensuse/build.sh
