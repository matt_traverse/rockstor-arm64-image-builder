#!/bin/bash
set -e

print_reminder() {
  echo
  echo "It is important to know that this is a preview - upgrading to future"
  echo "packaged versions of Rockstor from a source install is _not supported_."
  echo
  echo "As the source version has not gone through the QA process for testing and release"
  echo "it does contain bugs and there is no guarantee about it's safety."
  echo
  echo "In lieu of being able to subscribe to the stable channel, please consider"
  echo "making a donation to the project from the \"Donate\" button in the web UI."
}

echo "This script will download and setup Rockstor from _source_"

print_reminder

echo "Starting download and compile in 10 seconds"
sleep 10

cd /opt
git clone --branch arm64-patches https://github.com/mcbridematt/rockstor-core
cd rockstor-core
git remote add rockstor https://github.com/rockstor/rockstor-core
git fetch -a rockstor
git rebase rockstor/master
cd ..

python /opt/rockstor-core/bootstrap.py -c /opt/rockstor-core/buildout.cfg
/opt/rockstor-core/bin/buildout -N -c /opt/rockstor-core/buildout.cfg

echo "Compile and install process complete"
echo "You may need to wait a few minutes while Rockstor starts up."
echo
echo "As a reminder.. "
print_reminder
